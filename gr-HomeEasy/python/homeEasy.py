#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2017 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy as np
from gnuradio import gr

class homeEasy(gr.interp_block):
    """
    docstring for block homeEasy
    """
    def __init__(self,UPTIME=275,DOWNTIME0=300,DOWNTIME1=1335,DOWNTIMEHEAD=2840,samplerate=1000000):
		self.samplerate = samplerate
		self.uptime = int(round(UPTIME*samplerate/1000000))
		self.downtime0 = int(round(DOWNTIME0*samplerate/1000000))
		self.downtime1 = int(round(DOWNTIME1*samplerate/1000000))
		self.sample_nb =  2*self.uptime + self.downtime0 + self.downtime1
		self.downtimehead = int(round(DOWNTIMEHEAD*samplerate/1000000))
		self.zero = np.zeros(self.sample_nb, dtype=np.float32)
		self.zero[0 : self.uptime] = 1
		self.zero[self.uptime + self.downtime0 : (2*self.uptime) + self.downtime0] = 1
		self.one = np.zeros(self.sample_nb, dtype=np.float32)
		self.one[0 : self.uptime] = 1
		self.one[self.uptime + self.downtime1 : (2*self.uptime) + self.downtime1] = 1
		self.head1 = np.zeros(self.sample_nb, dtype=np.float32)
		self.head2 = np.zeros(self.sample_nb, dtype=np.float32)
		if self.downtimehead>=self.sample_nb :
			if (self.downtimehead + self.uptime)<(2*self.sample_nb) :
				self.head1[(2*self.sample_nb) - (self.downtimehead+self.uptime) : (2*self.sample_nb) - (self.downtimehead)] = 1
			else:
				print("header too long,add a third header in code")
			head1[0 : self.uptime] = 1
		else:
			if (self.downtimehead+self.uptime>self.sample_nb):
				head1[(2*self.sample_nb) - (self.downtimehead + self.uptime) : self.sample_nb] = 1
				head2[0 : self.sample_nb - self.downtimehead] = 1
			else:
				head2[(self.sample_nb) - (self.downtimehead+self.uptime) : (self.sample_nb) - (self.downtimehead)] = 1
		self.footer = np.zeros(self.sample_nb, dtype=np.float32)
		self.footer[0 : self.uptime] = 1
		self.defaul = np.zeros(self.sample_nb, dtype=np.float32)
		gr.interp_block.__init__(self,
            name="homeEasy",
            in_sig=[np.int8],
            out_sig=[np.float32], interp=self.sample_nb)

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]
        # <+signal processing here+>
        for j in range(len(in0)):
		if in0[j] == 0:
			out[j*self.sample_nb:(j+1)*self.sample_nb] = self.zero
		elif in0[j] == 1:
 	   		out[j*self.sample_nb:(j+1)*self.sample_nb] = self.one
		elif in0[j] == 2:
			out[j*self.sample_nb:(j+1)*self.sample_nb] = self.head1
		elif in0[j] == 3:
			out[j*self.sample_nb:(j+1)*self.sample_nb] = self.head2
		elif in0[j] == 4:
			out[j*self.sample_nb:(j+1)*self.sample_nb] = self.footer
		else:
			out[j*self.sample_nb:(j+1)*self.sample_nb] = self.defaul
        return len(output_items[0])

