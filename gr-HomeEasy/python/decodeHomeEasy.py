#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2017 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy as np
from gnuradio import gr

class decodeHomeEasy(gr.sync_block):
    """
    docstring for block decodeHomeEasy
    """
    def __init__(self,samplerate=1000000,errormargin=0.2,threshold=0.3,filterlength=4):
		self.compteur_changestate = 0
		self.state = 0
		self.hightlow_state = False
		self.Threshold = threshold
		self.FILTERLENGTH = filterlength
		self.hightlow_vector = np.zeros(self.FILTERLENGTH, dtype=np.float32)
		self.vector = np.zeros(32, dtype=np.bool)
		self.indice_vector = 0
		self.enable = False
		self.longtram = int(round(1660*samplerate/1000000))
		self.shorttram = int(round(575*samplerate/1000000))
		self.headerFrame = int(round(3117*samplerate/1000000))
		self.ERRORMARGIN = errormargin
		self.headerlow = self.headerFrame*(1 - self.ERRORMARGIN)
		self.headerhigh = self.headerFrame*(1 + self.ERRORMARGIN)
		self.shortframelow = self.shorttram*(1 - self.ERRORMARGIN)
		self.shortframehigh = self.shorttram*(1 + self.ERRORMARGIN)
		self.longframelow = self.longtram*(1 - self.ERRORMARGIN)
		self.longframehigh = self.longtram*(1 + self.ERRORMARGIN)
		gr.sync_block.__init__(self,
            name="decodeHomeEasy",
            in_sig=[np.float32],
            out_sig=[(np.bool,32),np.bool])

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]
        out1 = output_items[1]
        # <+signal processing here+>
        for j in range(len(in0)):
			self.hightlow_vector[1 : self.FILTERLENGTH] = self.hightlow_vector[0 : self.FILTERLENGTH-1]
			self.hightlow_vector[0] = in0[j]
			self.compteur_changestate += 1
			if ((not self.hightlow_state) and np.all(self.hightlow_vector > self.Threshold) ):
				self.hightlow_state = True
			if (self.hightlow_state and np.all(self.hightlow_vector < self.Threshold)):
				self.hightlow_state = False
				if(self.state == 0):
					if(self.headerlow < self.compteur_changestate < self.headerhigh):
						self.state = 1
						self.compteur_changestate = 0
					else:
						self.state = 0
						self.compteur_changestate = 0
				elif(self.state == 1):
					if(self.shortframelow < self.compteur_changestate < self.shortframehigh):
						if(self.indice_vector == 31):
							self.vector[31] = 0
							self.enable = True
							self.state = 0
							self.compteur_changestate = 0
							self.indice_vector = 0
						else:
							self.state = 2
							self.compteur_changestate = 0
					elif(self.longframelow < self.compteur_changestate < self.longframehigh):
						if(self.indice_vector == 31):
							self.vector[31] = 1
							self.enable = True
							self.state = 0
							self.compteur_changestate = 0
							self.indice_vector = 0
						else:
							self.state = 3
							self.compteur_changestate = 0
					else:
						self.state = 0
						self.compteur_changestate = 0
						self.vector[:] = 0
						self.indice_vector = 0
				elif(self.state == 2):
					if(self.longframelow < self.compteur_changestate < self.longframehigh):
						self.vector[self.indice_vector] = 0
						self.indice_vector += 1
						self.state = 1
						self.compteur_changestate = 0
					else:
						self.state = 0
						self.compteur_changestate = 0
						self.vector[:] = 0
						self.indice_vector = 0
				elif(self.state == 3):
					if(self.shortframelow < self.compteur_changestate < self.shortframehigh):
						self.vector[self.indice_vector] = 1
						self.indice_vector += 1
						self.state = 1
						self.compteur_changestate = 0
					else:
						self.state = 0
						self.compteur_changestate = 0
						self.vector[:] = 0
						self.indice_vector = 0
			out0[j] = self.vector
			out1[j] = self.enable
			if(self.enable):
				self.vector[:] = 0	
				self.enable = False
			return len(output_items[0])

