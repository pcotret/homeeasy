#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2017 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy as np
from gnuradio import gr

class decodeFrameHomeEasy(gr.sync_block):
    """
    docstring for block decodeFrameHomeEasy
    """
    def __init__(self):
		self.id=0
		self.flag=0
		self.onoff=0
		self.device=0
		self.enable=0
		self.binary25 = 2 ** np.arange(25 , -1 , -1 , dtype = np.float32)
		self.binary4 = 2 ** np.arange(3 , -1 , -1 , dtype = np.int8)
		gr.sync_block.__init__(self,
            name="decodeFrameHomeEasy",
            in_sig=[(np.bool,32),np.bool],
            out_sig=[np.float32,np.bool,np.bool,np.int8])


    def work(self, input_items, output_items):
        frame = input_items[0]
        enable = input_items[0]
        out0 = output_items[0]
        out1 = output_items[1]
        out2 = output_items[2]
        out3 = output_items[3]
        # <+signal processing here+>
        for j in range(len(frame)):
			if(enable[j]):
				self.id = np.dot(frame[j][0 : 26] , self.binary25)
				self.flag = frame[j][26]
				self.onoff = frame[j][27]
				self.device = np.dot(frame[j][28 : 32] , self.binary4)
			out0[j] = self.id
			out1[j] = self.flag
			out2[j] = self.onoff
			out3[j] = self.device
        return len(output_items[0])

