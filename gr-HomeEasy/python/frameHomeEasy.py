#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2017 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy
from gnuradio import gr

class frameHomeEasy(gr.interp_block):
    """
    docstring for block frameHomeEasy
    """
    def bitfield(self,n,length):
		temp = [1 if digit=='1' else 0 for digit in bin(n)[2:]]
		return np.concatenate( (np.zeros(length-len(temp)),temp) , axis=0)
    
    def __init__(self):
		self.trame = np.zeros(38, dtype=np.int8)
		self.id_b = self.bitfield(iden,26)
		self.flag_b = self.bitfield(flag,1)
		self.devicecode_b = self.bitfield(devicecode,4)
		self.trame [0 : 2] = [2 , 3]
		self.trame [2 : 28] = self.id_b
		self.trame [28 : 29] = self.flag_b
		self.trame [30 : 34] = self.devicecode_b
		self.trame [34 : 35] = 4
		self.trame [35 : 38] = -1
		gr.interp_block.__init__(self,
            name="frameHomeEasy",
            in_sig=[np.int8],
            out_sig=[np.int8], interp=38)

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]
        # <+signal processing here+>
        for j in range(len(in0)):
		if (in0[j]>=0):
			self.trame [29] = in0[j]
			out[j*38:(j+1)*38] = self.trame
		else:
			out[j*38:(j+1)*38] = -1
		return len(output_items[0])

