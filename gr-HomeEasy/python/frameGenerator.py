#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2017 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy as np
from gnuradio import gr

class frameGenerator(gr.interp_block):
    """
    docstring for block frameGenerator
    """
    def __init__(self):
		if(nbrframe > delaytime):
			self.NbrFrame = delaytime
			print("NbrFrame is superior to Delaytime")
		else:
			self.NbrFrame = nbrframe
		self.InterpFactor = delaytime
		self.outFrame = np.zeros(self.InterpFactor, dtype=np.int8)
		gr.interp_block.__init__(self,
            name="frameGenerator",
            in_sig=[np.bool],
            out_sig=[np.int8], interp = self.InterpFactor)

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]
        # <+signal processing here+>
        out0.fill(-1)
        for j in range(len(in0)):
			out0[j*self.InterpFactor : j*self.InterpFactor + self.NbrFrame] = in0[j]
        return len(output_items[0])
