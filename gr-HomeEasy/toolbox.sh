#! /bin/sh -
PROGNAME=$0

usage() {
  cat << EOF >&2
Usage: $PROGNAME [-c <command>] [-m <module>] [-b <block>]
-c <command>: build, clean, update, rebuild, remove_module, remove_block
-m <module>: module name
-b <block>: block name
Examples:
$PROGNAME -c build: build the current module
$PROGNAME -c remove_module -m hello: uninstall the module hello (with all blocks !)
$PROGNAME -c remove_module -m hello -b world: uninstall the block world from module hello
EOF
  exit 1
}
if [ $# -eq 0 ]
  then
	usage
	exit 1
else
	module=default_module block=default_block
	while getopts m:b:c: o; do
		case $o in
			(c) command=$OPTARG;;
			(m) module=$OPTARG;;
			(b) block=$OPTARG;;
			(*) usage;;
		esac
	done
	shift "$((OPTIND - 1))"
	case $command in
		build)
			mkdir build
			cd build && cmake .. && make && sudo make install && sudo ldconfig
			;;
		clean)
			rm -rf build
			;;
		update)
			cd build && make && sudo make install && sudo ldconfig
			;;
		rebuild)
			rm -rf build
			mkdir build
			cd build && cmake .. && make && sudo make install && sudo ldconfig
			;;
		remove_module)
			string="gr-"
			string="$string$module"
			sudo rm -r /usr/local/lib/cmake/$module
			sudo rm -r /usr/local/include/$module
			sudo rm -r /usr/local/lib/python2.7/dist-packages/$module
			sudo rm /usr/local/share/gnuradio/grc/blocks/$module*.xml
			sudo rm -r /usr/local/share/doc/$string
			sudo rm -rf build
			;;
		remove_block)
			$0 -c remove_module -m $module
			gr_modtool remove $block
			$0 -c rebuild
			;;
	esac
fi
