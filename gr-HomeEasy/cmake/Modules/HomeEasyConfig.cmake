INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_HOMEEASY HomeEasy)

FIND_PATH(
    HOMEEASY_INCLUDE_DIRS
    NAMES HomeEasy/api.h
    HINTS $ENV{HOMEEASY_DIR}/include
        ${PC_HOMEEASY_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    HOMEEASY_LIBRARIES
    NAMES gnuradio-HomeEasy
    HINTS $ENV{HOMEEASY_DIR}/lib
        ${PC_HOMEEASY_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(HOMEEASY DEFAULT_MSG HOMEEASY_LIBRARIES HOMEEASY_INCLUDE_DIRS)
MARK_AS_ADVANCED(HOMEEASY_LIBRARIES HOMEEASY_INCLUDE_DIRS)

